package matti.crypto;

import org.apache.commons.codec.binary.Base64;

public class Base64C extends Crypto {

	public Base64C(String string) {
		super(string);
	}
	public String encrypt() {
		return Base64.encodeBase64String(this.bytes);
	}
	
	public String decrypt() {
		if (this.isCorrect()) {
			byte[] resultB = Base64.decodeBase64(this.string);
			return new String(resultB); 
		} else {
			return "ERROR";
		}
	}
	public boolean isCorrect() {
		if (Base64.isBase64(this.bytes)) 
			return true;
		else
			return false;
	}
	
}
