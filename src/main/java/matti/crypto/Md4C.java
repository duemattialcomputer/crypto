package matti.crypto;

import org.apache.commons.codec.binary.Hex;

import jcifs.util.MD4;

public class Md4C extends Crypto {

    public Md4C(String string) {
        super(string);
    }
    public String encode() {
        MD4 md4 = new MD4();
        for (byte bit : this.bytes) {
            md4.engineUpdate(bit);
        }
        byte[] result = md4.engineDigest();
        
        return Hex.encodeHexString(result);
    }
}