package matti.crypto;

import org.apache.commons.codec.digest.DigestUtils;

public class Md5C extends Crypto {

	public Md5C(String string) {
		super(string);
	}

	public String encode() {
		return DigestUtils.md5Hex(this.string).toUpperCase();
	}

}
