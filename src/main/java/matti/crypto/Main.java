package matti.crypto;

public class Main {

	public static void main(String[] args) throws Throwable {
        String passwd = "P4ssW0RdL337";
        System.out.println("Password: " + passwd + "\n");
        DESC crypt = new DESC(passwd, "READLINKEY");
        System.out.print("Crypto: ");
        if (crypt.encode().length() > 48)
            System.out.println();
        System.out.println(crypt.encode().length());
	}

}
